#!/bin/bash

for i in {0..2}; do
    echo "Users for partition $i..."
    ./test-ssn $1 | grep "USER " | cut -f 2,5 -d ' '| awk -v part="$i" '$2 == part {print $1}' > part${i}.txt; 
done
echo "...done"
