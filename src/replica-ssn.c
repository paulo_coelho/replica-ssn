/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "shared-queue.h"
#include "uthash.h"
#include <assert.h>
#include <errno.h>
#include <errno.h>
#include <evamcast.h>
#include <event2/buffer.h>
#include <event2/event.h>
#include <event2/thread.h>
#include <mcast.h>
#include <message_mcast.h>
#include <netinet/tcp.h>
#include <peers_mcast.h>
#include <pthread.h>
#include <signal.h>
#include <ssn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define QUEUE_SIZE 10000
#define MAX_VALUE_SIZE 8192
#define STAT_INTERVAL 5 // interval to show stats (in seconds)

struct connected_client
{
  int                 unique_client_id; // the hashtable key
  struct bufferevent* bev;
  UT_hash_handle      hh;
};

struct client_value
{
  unsigned       unique_client_id; // client unique identifier
  struct timeval t;                // client timestamp
  short          part_nr;          // number of partitions required to run
  size_t         size;             // size of the value in bytes
  short command;  // 1: GET_TIMELINE; 2: POST; 4: FOLLOW;  8: MIGRATE_USER
  int   user_id;  // 1: USER_ID     ; 2: --- ; 4: USER_ID; 8: PARTITION "TO"
  char  value[0]; // 1: -------     ; 2: MSG ; 4: -------; 8: USER_ID LIST
};

struct consumer_amcast
{
  pthread_t                 t;
  int                       group;
  struct replica_amcast*    parent;
  struct connected_client** clients;
  pthread_mutex_t*          mutex;
  pthread_cond_t*           sync_cond;
  m_ts_t                    ts;
  m_uid_t                   uid;
  shared_queue*             q;
  long                      count;
  struct client_value*      response;
};

struct producer_amcast
{
  pthread_t              t;
  int                    group;
  struct bufferevent*    bev;
  struct event_base*     base;
  struct event*          reconnect_ev;
  struct timeval         reconnect_timeout;
  struct evmcast_config* conf;
  struct replica_amcast* parent;
  shared_queue*          q;
  long                   count;
};

struct replica_amcast
{
  int                     id;
  int                     my_group;
  int                     group_count;
  const char*             conf;
  const char*             pxconf;
  struct mcast_peers*     peers;
  struct event_base*      base;
  struct event_base*      amcast_base;
  struct producer_amcast* prod;  // one per group: send client messages to
                                 // multicast service and enqueues responses
  struct consumer_amcast*  cons; // one per group: replies to clients
  struct connected_client* clients;
  struct ssn_t*            ssn; // the social network
  pthread_mutex_t*         post_mutex;
};

static int           verbose = 0;
static int           stop_consuming = 0;
static struct ssn_t* ssn; // the reference used for conflict checking
pthread_mutex_t*     ssn_mutex;

static void
print_stats(evutil_socket_t fd, short event, void* arg)
{
  struct replica_amcast* r = arg;
  int                    i;

  for (i = 0; i < r->group_count; i++) {
    printf("Producer %d: %ld msgs/s\n", i, r->prod[i].count / STAT_INTERVAL);
    r->prod[i].count = 0;
    printf("Consumer %d: %ld msgs/s\n\n", i, r->cons[i].count / STAT_INTERVAL);
    r->cons[i].count = 0;
  }
}

/**********************************************
 * CLIENT-RELATED TASKS    v v v v v v v      *
 **********************************************/
static int
add_client(struct connected_client** clients, int unique_client_id,
           struct bufferevent* bev)
{
  struct connected_client* cc;
  HASH_FIND_INT(*clients, &unique_client_id,
                cc); /* unique_client_id already in the hash? */
  if (cc == NULL) {
    cc = malloc(sizeof(struct connected_client));
    cc->unique_client_id = unique_client_id;
    cc->bev = bev;
    HASH_ADD_INT(*clients, unique_client_id,
                 cc); /* unique_client_id: name of key field */
    return 1;
  }
  return 0;
}

static struct connected_client*
get_client(struct connected_client** clients, int unique_client_id)
{
  struct connected_client* cc;
  HASH_FIND_INT(*clients, &unique_client_id,
                cc); /* unique_client_id already in the hash? */
  return cc;
}

void
del_client_all(struct connected_client** clients)
{
  struct connected_client *cc, *tmp;
  HASH_ITER(hh, *clients, cc, tmp)
  {
    HASH_DEL(*clients, cc); /* delete; clients advances to next */
    free(cc);               /* optional- if you want to free  */
  }
}

static void
handle_clients(struct mcast_peer* p, mcast_message* m, void* arg)
{
  struct replica_amcast* r = arg;
  struct client_value*   v = (struct client_value*)m->value.mcast_value_val;
  int                    dst_group, i, *fp;
  if (verbose)
    printf("NEW MESSAGE: %d bytes (%.16s) from client %u, preparing for "
           "multicast...\n",
           m->value.mcast_value_len, v->value, v->unique_client_id);
  // populate destination groups!
  // 1: GET_TIMELINE; 2: POST; 4: FOLLOW;  8: MIGRATE_USER
  switch (v->command) {
    case 1:
      m->to_groups_len = 1;
      m->to_groups[0] = ssn_get_partition(r->ssn, v->user_id);
      dst_group = m->to_groups[0];
      break;
    case 2:
      m->to_groups_len = ssn_get_follower_partitions(r->ssn, v->user_id, &fp);
      dst_group = fp[0];
      m->to_groups[0] = fp[0];
      for (i = 1; i < m->to_groups_len; i++) {
        m->to_groups[i] = fp[i];
        if (fp[i] == r->my_group)
          dst_group = fp[i];
      }
      break;
    default:
      printf("Command not in use: %d.\n", v->command);
      return;
  }
  v->part_nr = m->to_groups_len;
  if (verbose) {
    printf("PREPARED MESSAGE: command %d for user %d with destination to %d "
           "groups: ",
           v->command, v->user_id, m->to_groups_len);
    for (i = 0; i < m->to_groups_len; i++)
      printf(" %d", m->to_groups[i]);
    printf("\n");
  }
  add_client(&r->clients, v->unique_client_id, mcast_peer_get_buffer(p));
  mcast_submit(r->prod[dst_group].bev, m->value.mcast_value_val,
               m->value.mcast_value_len, m->to_groups, m->to_groups_len);
}

/**********************************************
 * PRODUCER  v v v v v v v v v v v v v v      *
 **********************************************/

static void
on_connect(struct bufferevent* bev, short events, void* arg)
{
  struct producer_amcast* p = arg;
  if (events & BEV_EVENT_CONNECTED) {
    printf("Connected to group/node %d/%d...\n", p->group, p->parent->id);
    mcast_register_replica(p->bev, p->parent->my_group, p->parent->id);
  } else {
    printf("Socket ERROR: %s\n",
           evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
    bufferevent_free(p->bev);
    p->bev = NULL;
    event_add(p->reconnect_ev, &p->reconnect_timeout);
  }
}

static void
enqueue_msg(struct bufferevent* client, mcast_message* m, void* arg)
{
  struct producer_amcast* p = arg;
  shared_queue*           fifo = p->q;
  mcast_message*          aux;
  aux = malloc(sizeof(mcast_message));

  mcast_message_copy(aux, m, 1);
  pthread_mutex_lock(fifo->mut);
  while (fifo->full) {
    printf("PRODUCER %d: queue FULL.\n", p->group);
    pthread_cond_wait(fifo->not_full, fifo->mut);
  }
#ifndef NDEBUG
  struct client_value* to_queue;
  if (verbose) {
    to_queue = (struct client_value*)aux->value.mcast_value_val;
    printf("PRODUCER %d received %d bytes - value size is %d\n", p->group,
           aux->value.mcast_value_len, to_queue->size);
  }
#endif // NDEBUG
  shared_queue_add(fifo, aux);
  p->count++;
  pthread_mutex_unlock(fifo->mut);
  pthread_cond_signal(fifo->not_empty);
}

static void
produce(struct bufferevent* bev, void* arg)
{
  struct producer_amcast* p = arg;
  mcast_message           msg;
  struct evbuffer*        buf = bufferevent_get_input(bev);

  while (recv_mcast_message(buf, &msg)) {
    enqueue_msg(bev, &msg, p);
  }
}

static void
connect_to_node(struct producer_amcast* p)
{
  struct sockaddr_in addr =
    evmcast_node_address(p->conf, p->group, p->parent->id);
  p->bev = bufferevent_socket_new(p->base, -1,
                                  BEV_OPT_CLOSE_ON_FREE | BEV_OPT_THREADSAFE);
  bufferevent_setcb(p->bev, produce, NULL, on_connect, p);
  bufferevent_enable(p->bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(p->bev, (struct sockaddr*)&addr, sizeof(addr));
  int flag = 1;
  setsockopt(bufferevent_getfd(p->bev), IPPROTO_TCP, TCP_NODELAY, &flag,
             sizeof(int));
}

static void
on_connection_timeout(int fd, short ev, void* arg)
{
  struct producer_amcast* p = arg;
  connect_to_node(p);
}

static void*
create_producer(void* arg)
{
  struct producer_amcast* p = arg;
  p->base = event_base_new();
  p->reconnect_ev = evtimer_new(p->base, on_connection_timeout, p);
  connect_to_node(p);
  event_base_dispatch(p->base);
  printf("PRODUCER %d: exiting...\n", p->group);
  pthread_exit(NULL);
}

/**********************************************
 * CONSUMER  v v v v v v v v v v v v v v      *
 **********************************************/
static int
can_deliver(int consumer_group, mcast_message* m, struct replica_amcast* r)
{
  int     i = 0, group, group_count = m->to_groups_len;
  m_ts_t  ts = m->timestamp;
  m_uid_t uid = m->uid;

  for (; i < group_count; i++) {
    group = m->to_groups[i];
#ifdef NDEBUG
    if (verbose)
      printf("CONSUMER %d... checking group %d with ts %u and uid %llu\n",
             consumer_group, group, r->cons[group].ts, r->cons[group].uid);
#endif // !NDEBUG
    if (r->cons[group].ts < ts)
      return 0;
    if (r->cons[group].ts == ts && r->cons[group].uid < uid)
      return 0;
  }
  return 1;
}

void
run_command(struct replica_amcast* r, struct client_value* c,
            struct client_value* resp)
{
  struct ssn_t*     ssn = r->ssn;
  int               command = c->command, user = c->user_id, size = 0, i = 0;
  int*              migrated_users = NULL;
  struct ssn_msg_t* timeline = NULL;

  memcpy(resp, c, sizeof(struct client_value));

  if (verbose)
    printf("Running command %d, user_id %d", resp->command, resp->user_id);

  switch (command) {
    case 1:
      size = ssn_get_timeline(ssn, user, &timeline);
      resp->size = size * sizeof(struct ssn_msg_t) > MAX_VALUE_SIZE
                     ? MAX_VALUE_SIZE
                     : size * sizeof(struct ssn_msg_t);
      if (size)
        memcpy(resp->value, timeline, resp->size);
      break;
    case 2:
      pthread_mutex_lock(r->post_mutex);
      ssn_post(ssn, user, c->value, c->size);
      pthread_mutex_unlock(r->post_mutex);
      resp->size = 0;
      break;
    case 4:
      ssn_follow(ssn, user, atoi(c->value));
      resp->size = 0;
      break;
    case 8:
      size = c->size / sizeof(int); // total of users to migrate
      migrated_users = (int*)c->value;
      for (i = 0; i < size; i++) {
        // c->user_id is the new partition of each user in migrated_users
        ssn_set_partition(ssn, migrated_users[i], c->user_id);
      }
      resp->size = 0;
      break;
    default:
      printf("Invalid command code: %d. Doing nothing.\n", command);
      resp->size = 0;
  }
  if (verbose)
    printf("Running command %d, user_id %d", resp->command, resp->user_id);
}

static void*
consume(void* arg)
{
  struct consumer_amcast*  c = arg;
  struct client_value*     v = NULL;
  struct connected_client* cc = NULL;
  mcast_message*           m;
  shared_queue*            fifo = c->q;

  while (!stop_consuming) {
    pthread_mutex_lock(fifo->mut);
    while (fifo->empty && !stop_consuming) {
      if (verbose)
        printf("CONSUMER %d: queue EMPTY.\n", c->group);
      pthread_cond_wait(fifo->not_empty, fifo->mut);
    }
    if (stop_consuming) {
      printf("CONSUMER %d: exiting...\n", c->group);
      break;
    }
    shared_queue_del(fifo, (void**)&m);
    v = (struct client_value*)m->value.mcast_value_val;
    pthread_mutex_unlock(fifo->mut);
    pthread_cond_signal(fifo->not_full);

    if (verbose)
      printf("CONSUMER %d: received (%u, %llu), |dst| = %d from multicast for "
             "client %u.\n",
             c->group, m->timestamp, m->uid, m->to_groups_len,
             v->unique_client_id);

    if (m->to_groups_len > 1) {
      if (c->ts < m->timestamp) {
        c->ts = m->timestamp;
        c->uid = m->uid;
      } else if (c->ts == m->timestamp && c->uid < m->uid) {
        c->uid = m->uid;
      }

      pthread_cond_signal(c->sync_cond);
      pthread_mutex_lock(c->mutex);
#ifdef NDEBUG
      if (verbose)
        printf("CONSUMER %d: checking message (%u, %llu) inside critical "
               "region...\n",
               c->group, m->timestamp, m->uid);
#endif // !NDEBUG

      while (!can_deliver(c->group, m, c->parent)) {
#ifdef NDEBUG
        if (verbose)
          printf("CONSUMER %d: waiting (releasing lock)...\n", c->group);
#endif // !NDEBUG

        pthread_cond_signal(c->sync_cond);
        pthread_cond_wait(c->sync_cond, c->mutex);
      }
      pthread_mutex_unlock(c->mutex);
      pthread_cond_signal(c->sync_cond);
#ifdef NDEBUG
      if (verbose)
        printf("CONSUMER %d: message (%u, %llu) CHECKED, signaling\n", c->group,
               m->timestamp, m->uid);
#endif // !NDEBUG
    }
    // RUN and REPLY TO CLIENT
    if (c->group == m->to_groups[0]) {
      run_command(c->parent, v, c->response);
      cc = get_client(c->clients, v->unique_client_id);
      if (cc != NULL) {
        if (verbose)
          printf("CONSUMER %d: sending response to client %d for command %d, "
                 "user %d\n",
                 c->group, c->response->unique_client_id, c->response->command,
                 c->response->user_id);
        if (cc->bev != NULL)
          bufferevent_write(cc->bev, (char*)c->response,
                            sizeof(struct client_value) + c->response->size);
      }
    }
    mcast_message_free(m);
    c->count++;
  }
  pthread_exit(NULL);
}

/**********************************************
 * AMCAST    v v v v v v v v v v v v v v      *
 **********************************************/
static void
do_nothing(struct bufferevent* client, mcast_message* m, void* arg)
{
}

static int
conflict_check(char* left_msg, char* right_msg, int size)
{
  int                  conflict = 1;
  struct client_value *lc = (struct client_value *)left_msg,
                      *rc = (struct client_value *)right_msg;

  if (lc->command == 1 && rc->command == 1)
    return 0;
  if (lc->command == 2 && rc->command == 2) {
    pthread_mutex_lock(ssn_mutex);
    conflict = ssn_has_common_followers(ssn, lc->user_id, rc->user_id);
    pthread_mutex_unlock(ssn_mutex);
    return conflict;
  }
  printf("Situation not checked, assuming as conflicting!\n");
  return 1;
}

void*
run_amcast(void* arg)
{
  struct replica_amcast*  r = arg;
  struct event_base*      base;
  struct evamcast_node*   node;
  mcast_deliver_function  cb = do_nothing;
  conflict_check_function cf = conflict_check;

  base = event_base_new();
  node = evamcast_node_init(r->my_group, r->id, r->conf, r->pxconf, cb, NULL,
                            cf, base);
  if (node == NULL) {
    printf("Could not start the node!\n");
    exit(1);
  }

  r->amcast_base = base;
  ssn = r->ssn;
  ssn_mutex = r->post_mutex;
  event_base_dispatch(base);
  evamcast_node_free(node);
  event_base_free(base);
  pthread_exit(NULL);
}

/**********************************************
 * REPLICA   v v v v v v v v v v v v v v      *
 **********************************************/
static void
handle_sigint(int sig, short ev, void* arg)
{
  int                    i;
  struct replica_amcast* r = arg;
  struct event_base*     base = r->base;
  printf("Caught signal %d... \n", sig);
  if (sig == SIGINT) {
    event_base_loopexit(base, NULL);
    stop_consuming = 1;
    for (i = 0; i < r->group_count; i++) {
      pthread_cond_signal(r->prod[i].q->not_empty);
      event_base_loopexit(r->prod[i].base, NULL);
    }
    pthread_cond_broadcast(r->cons[0].sync_cond);
    event_base_loopexit(r->amcast_base, NULL);
  }
}

static struct replica_amcast*
replica_init(int group, int id, int port, struct event_base* base,
             const char* config_file, const char* pxconfig_file,
             const char* json_file)
{
  struct replica_amcast* r;
  struct evmcast_config* config = evmcast_config_read(config_file);
  struct mcast_peers*    mcast_peers = mcast_peers_new(base, NULL);
  int              group_count, i, rv = mcast_peers_listen(mcast_peers, port);
  pthread_mutex_t* mutex = malloc(sizeof(pthread_mutex_t));
  pthread_cond_t*  sync_cond = malloc(sizeof(pthread_cond_t));

  if (rv == 0 || config == NULL)
    return NULL;

  group_count = evmcast_group_count(config);

  r = malloc(sizeof(struct replica_amcast));
  r->id = id;
  r->my_group = group;
  r->group_count = group_count;
  r->peers = mcast_peers;
  r->base = base;
  r->conf = config_file;
  r->pxconf = pxconfig_file;
  r->clients = NULL; // required by UTHASH
  r->ssn = ssn_load_from_json_file(json_file);

  pthread_mutex_init(mutex, NULL);
  pthread_cond_init(sync_cond, NULL);
  r->cons = malloc(group_count * sizeof(struct consumer_amcast));
  r->prod = malloc(group_count * sizeof(struct producer_amcast));
  for (i = 0; i < group_count; i++) {
    // consumer
    r->cons[i].group = i;
    r->cons[i].parent = r;
    r->cons[i].q = shared_queue_new(QUEUE_SIZE);
    r->cons[i].clients = &r->clients;
    r->cons[i].ts = 0;
    r->cons[i].uid = 0;
    r->cons[i].count = 0;
    r->cons[i].mutex = mutex;
    r->cons[i].sync_cond = sync_cond;
    r->cons[i].response = malloc(sizeof(struct client_value) + MAX_VALUE_SIZE);
    // producer
    r->prod[i].group = i;
    r->prod[i].parent = r;
    r->prod[i].q = r->cons[i].q;
    r->prod[i].conf = config;
    r->prod[i].count = 0;
    r->prod[i].bev = NULL;
    r->prod[i].reconnect_timeout.tv_sec = 2;
    r->prod[i].reconnect_timeout.tv_usec = 0;
  }

  for (i = 0; i < group_count; i++) {
    pthread_create(&r->prod[i].t, NULL, create_producer, &r->prod[i]);
    pthread_create(&r->cons[i].t, NULL, consume, &r->cons[i]);
  }

  mcast_peers_subscribe(mcast_peers, MCAST_CLIENT, handle_clients, r);
  r->post_mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
  pthread_mutex_init(r->post_mutex, NULL);
  return r;
}

static void
replica_free(struct replica_amcast* r)
{
  int i;
  mcast_peers_free(r->peers);
  event_base_free(r->base);

  evmcast_config_free(r->prod[0].conf);
  for (i = 0; i < r->group_count; i++) {
    shared_queue_free(r->cons[i].q);
    free(r->cons[i].response);
    if (r->prod[i].bev != NULL)
      bufferevent_free(r->prod[i].bev);
    event_free(r->prod[i].reconnect_ev);
    event_base_free(r->prod[i].base);
  }
  pthread_mutex_destroy(r->cons[0].mutex);
  pthread_cond_destroy(r->cons[0].sync_cond);
  free(r->cons[0].mutex);
  free(r->cons[0].sync_cond);
  free(r->prod);
  free(r->cons);
  pthread_mutex_destroy(r->post_mutex);
  free(r->post_mutex);
  ssn_free(r->ssn);
  del_client_all(&r->clients);
  HASH_CLEAR(hh, r->clients);
  free(r);
}

static void
run_replica(int group, int id, int port, const char* conf, const char* pxconf,
            const char* json)
{
  int                    i;
  struct event_base*     base;
  struct event *         sig, *stat;
  struct replica_amcast* r;
  struct timeval         stat_interval = { STAT_INTERVAL, 0 };
  pthread_t              mct;

  base = event_base_new();
  r = replica_init(group, id, port, base, conf, pxconf, json);

  // amcast initialization
  pthread_create(&mct, NULL, run_amcast, r);

  sig = evsignal_new(base, SIGINT, handle_sigint, r);
  stat = event_new(base, -1, EV_PERSIST, print_stats, r);
  evsignal_add(sig, NULL);
  event_add(stat, &stat_interval);
  event_base_dispatch(base);
  event_free(sig);
  event_free(stat);

  for (i = 0; i < r->group_count; i++) {
    pthread_join(r->prod[i].t, NULL);
    pthread_join(r->cons[i].t, NULL);
  }

  pthread_join(mct, NULL);
  replica_free(r);
}

static void
usage(const char* prog)
{
  printf("Usage: %s  [path/to/mcast.conf] [path/to/paxos.conf] "
         "[path/to/social-network.json] -i "
         "<id> -g <group> -p <port> [-v] [-h]\n",
         prog);
  printf("  %-30s%s\n", "-i, -replica-id #", "ID of this replica");
  printf("  %-30s%s\n", "-g, -local-group #", "Local group of this replica");
  printf("  %-30s%s\n", "-p, --port #", "Port to listen to");
  printf("  %-30s%s\n", "-h, --help", "Output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose",
         "Print delivered messages information");
  exit(1);
}

int
main(int argc, char const* argv[])
{
  int         id = 0, group = 0, port = 9002;
  const char *config = NULL, *pxconfig = NULL, *ssn_json = NULL;
  int         i = 1;

  if (argc >= 10 && argv[0][0] != '-' && argv[1][0] != '-') {
    config = argv[i++];
    pxconfig = argv[i++];
    ssn_json = argv[i++];
  } else
    usage(argv[0]);

  while (i != argc) {
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
      usage(argv[0]);
    else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0)
      verbose = 1;
    else if (strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "--replica-id") == 0)
      id = atoi(argv[++i]);
    else if (strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--port") == 0)
      port = atoi(argv[++i]);
    else if (strcmp(argv[i], "-g") == 0 ||
             strcmp(argv[i], "--local-group") == 0)
      group = atoi(argv[++i]);
    else
      usage(argv[0]);
    i++;
  }

  sigset_t mask;
  sigset_t orig_mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGPIPE);
  if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0) {
    perror("sigprocmask");
    return 1;
  }

  srand(time(NULL));
  i = evthread_use_pthreads();
  assert(i == 0);
  run_replica(group, id, port, config, pxconfig, ssn_json);
  return 0;
}
