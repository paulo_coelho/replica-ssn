/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "shared-queue.h"
#include <stdlib.h>
#include <unistd.h>

shared_queue*
shared_queue_new(int queue_size)
{
  shared_queue* q;

  q = malloc(sizeof(shared_queue));
  q->buf_size = queue_size;
  q->buf = malloc(queue_size * sizeof(void*));
  q->empty = 1;
  q->full = 0;
  q->head = 0;
  q->tail = 0;
  q->mut = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
  pthread_mutex_init(q->mut, NULL);
  q->not_full = (pthread_cond_t*)malloc(sizeof(pthread_cond_t));
  pthread_cond_init(q->not_full, NULL);
  q->not_empty = (pthread_cond_t*)malloc(sizeof(pthread_cond_t));
  pthread_cond_init(q->not_empty, NULL);

  return q;
}

void
shared_queue_free(shared_queue* q)
{
  pthread_mutex_destroy(q->mut);
  free(q->mut);
  pthread_cond_destroy(q->not_full);
  free(q->not_full);
  pthread_cond_destroy(q->not_empty);
  free(q->not_empty);
  free(q->buf);
  free(q);
}

void
shared_queue_add(shared_queue* q, void* in)
{
  q->buf[q->tail] = in;
  q->tail++;
  if (q->tail == q->buf_size)
    q->tail = 0;
  if (q->tail == q->head)
    q->full = 1;
  q->empty = 0;
}

void
shared_queue_del(shared_queue* q, void** out)
{
  *out = q->buf[q->head];
  q->head++;
  if (q->head == q->buf_size)
    q->head = 0;
  if (q->head == q->tail)
    q->empty = 1;
  q->full = 0;
  return;
}

/**
 * CONSUMER PRODUCER USAGE EXAMPLE
 *
 */

/*
#define QUEUE_SIZE 10
#define LOOP 20

static void *producer(void *);

static void *consumer(void *);

int
main() {
    shared_queue *fifo;
    pthread_t pro, con;

    fifo = shared_queue_new(QUEUE_SIZE);
    if (fifo == NULL) {
        fprintf(stderr, "main: Queue Init failed.\n");
        exit(1);
    }
    pthread_create(&pro, NULL, producer, fifo);
    pthread_create(&con, NULL, consumer, fifo);
    pthread_join(pro, NULL);
    pthread_join(con, NULL);
    shared_queue_free(fifo);

    return 0;
}

static void *
producer(void *q) {
    shared_queue *fifo = q;
    int i, *value;

    for (i = 0; i < LOOP; i++) {
        pthread_mutex_lock(fifo->mut);
        while (fifo->full) {
            printf("producer: queue FULL.\n");
            pthread_cond_wait(fifo->not_full, fifo->mut);
        }
        value = malloc(sizeof(int));
        *value = i;
        shared_queue_add(fifo, value);
        pthread_mutex_unlock(fifo->mut);
        pthread_cond_signal(fifo->not_empty);
        usleep(100000);
    }
    for (i = 0; i < LOOP; i++) {
        pthread_mutex_lock(fifo->mut);
        while (fifo->full) {
            printf("producer: queue FULL.\n");
            pthread_cond_wait(fifo->not_full, fifo->mut);
        }
        value = malloc(sizeof(int));
        *value = i;
        shared_queue_add(fifo, value);
        pthread_mutex_unlock(fifo->mut);
        pthread_cond_signal(fifo->not_empty);
        usleep(100000);
    }
    pthread_exit(NULL);
}

static void *
consumer(void *q) {
    shared_queue *fifo = q;
    int i, *d;

    for (i = 0; i < LOOP; i++) {
        pthread_mutex_lock(fifo->mut);
        while (fifo->empty) {
            printf("consumer: queue EMPTY.\n");
            pthread_cond_wait(fifo->not_empty, fifo->mut);
        }
        shared_queue_del(fifo,(void **) &d);
        pthread_mutex_unlock(fifo->mut);
        pthread_cond_signal(fifo->not_full);
        printf("consumer: received %d.\n", *d);
        free(d);
        usleep(200000);
    }
    for (i = 0; i < LOOP; i++) {
        pthread_mutex_lock(fifo->mut);
        while (fifo->empty) {
            printf("consumer: queue EMPTY.\n");
            pthread_cond_wait(fifo->not_empty, fifo->mut);
        }
        shared_queue_del(fifo,(void **) &d);
        pthread_mutex_unlock(fifo->mut);
        pthread_cond_signal(fifo->not_full);
        printf("consumer: received %d.\n", *d);
        free(d);
        usleep(500000);
    }
    pthread_exit(NULL);
}
*/