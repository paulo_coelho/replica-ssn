/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <evamcast.h>
#include <mcast.h>
#include <signal.h>
#include <ssn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct stats
{
  int            delivered;
  struct timeval last_tv;
  long           min_latency;
  long           max_latency;
  long           avg_latency;
};

struct stats_ctrl
{
  int            value_size;
  struct stats   stats;
  struct event*  stats_ev;
  struct timeval stats_interval;
};

// client value to check for conflict
#define MAX_VALUE_SIZE 8192 // bytes

struct client_value
{
  unsigned       unique_client_id; // client unique identifier
  struct timeval t;                // client timestamp
  short          part_nr;          // number of partitions required to run
  size_t         size;             // size of the value in bytes
  short command;  // 1: GET_TIMELINE; 2: POST   ; 4: FOLLOW ; 8: MIGRATE_USER
  int   user_id;  // 1: USER_ID     ; 2: USER_ID; 4: USER_ID; 8: PARTITION "TO"
  char  value[0]; // 1: -------     ; 2: MSG    ; 4: -------; 8: USER_ID LIST
};

static int       verbose = 0;
static long long count = 0;
struct ssn_t*    ssn;

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d\n", sig);
  if (sig == SIGINT)
    event_base_loopexit(base, NULL);
}

static void
on_stats_simple(evutil_socket_t fd, short event, void* arg)
{
  struct stats_ctrl* sc = arg;
  struct stats*      s = &(sc->stats);
  int                delta_t = (int)sc->stats_interval.tv_sec;
  double mbps = (double)(s->delivered * sc->value_size * 8) / (1024 * 1024);
  mbps /= delta_t;
  printf("STATS: %d msgs/sec, %.2lf Mbps\n", s->delivered / delta_t, mbps);
  s->delivered = 0;
  event_add(sc->stats_ev, &sc->stats_interval);
}

static void
on_deliver(struct bufferevent* client, mcast_message* m, void* arg)
{
  struct stats_ctrl* sc = arg;
  struct mcast_value val = m->value;

  sc->value_size = m->value.mcast_value_len;
  if (verbose) {
    printf("A-Deliver(m(%u, %llu)): - %ld bytes - order %lld\n", m->timestamp,
           m->uid, (long)sc->value_size, ++count);
  }
  if (client) {
    bufferevent_write(client, val.mcast_value_val, val.mcast_value_len);
  }
  sc->stats.delivered++;
}

static int
conflict_check(char* left_msg, char* right_msg, int size)
{
  int                  conflict = 1;
  struct client_value *lc = (struct client_value *)left_msg,
                      *rc = (struct client_value *)right_msg;

  if (lc->command == 1 && rc->command == 1)
    return 0;
  if (lc->command == 2 && rc->command == 2) {
    conflict = ssn_has_common_followers(ssn, lc->user_id, rc->user_id);
    return conflict;
  }
  printf("Situation not checked, assuming as conflicting!\n");
  return 1;
}

static void
start_node(int group_id, int id, const char* mconfig, const char* pconfig)
{
  struct event*           sig;
  struct event_base*      base;
  struct evamcast_node*   node;
  struct stats_ctrl       sc;
  mcast_deliver_function  cb = on_deliver;
  conflict_check_function cf = conflict_check;

  base = event_base_new();
  node = evamcast_node_init(group_id, id, mconfig, pconfig, cb, &sc, cf, base);
  if (node == NULL) {
    printf("Could not start the node!\n");
    exit(1);
  }

  memset(&(sc.stats), 0, sizeof(struct stats));
  gettimeofday(&sc.stats.last_tv, NULL);
  sc.stats_interval = (struct timeval){ 5, 0 };
  sc.stats_ev = evtimer_new(base, on_stats_simple, &sc);
  sc.value_size = 0;
  event_add(sc.stats_ev, &sc.stats_interval);

  sig = evsignal_new(base, SIGINT, handle_sigint, base);
  evsignal_add(sig, NULL);
  event_base_dispatch(base);

  event_free(sig);
  event_free(sc.stats_ev);
  evamcast_node_free(node);
  event_base_free(base);
}

static void
usage(const char* prog)
{
  printf("Usage: %s group_id node_id [path/to/mcast.conf] [path/to/paxos.conf] "
         "[path/to/ssn.json]"
         "[-h] [-v]\n",
         prog);
  printf("  %-30s%s\n", "-h, --help", "Output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose",
         "Print delivered messages information");
  exit(1);
}

int
main(int argc, char const* argv[])
{
  int         group_id, id;
  int         i = 3;
  const char* mconfig = "./scenario1.conf";
  const char* pconfig = " ./paxos.conf";
  const char* ssnjson = " ./ssn.json";

  if (argc < 4)
    usage(argv[0]);

  group_id = atoi(argv[1]);
  id = atoi(argv[2]);
  if (argc >= 4 && argv[3][0] != '-') {
    mconfig = argv[3];
    i++;
  }

  if (argc >= 5 && argv[4][0] != '-') {
    pconfig = argv[4];
    i++;
  }

  if (argc >= 6 && argv[5][0] != '-') {
    ssnjson = argv[5];
    i++;
  }

  while (i != argc) {
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
      usage(argv[0]);
    else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0)
      verbose = 1;
    else
      usage(argv[0]);
    i++;
  }
  
  sigset_t mask;
  sigset_t orig_mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGPIPE);
  if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0) {
    perror ("sigprocmask");
    return 1;
  }
  ssn = ssn_load_from_json_file(ssnjson);
  assert(ssn != NULL);
  srand(time(NULL));
  start_node(group_id, id, mconfig, pconfig);

  return 0;
}
