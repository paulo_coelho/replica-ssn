/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <event2/buffer.h>
#include <event2/event.h>
#include <event2/thread.h>
#include <evmcast.h>
#include <mcast.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <signal.h>
#include <ssn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define MAX_VALUE_SIZE 8192 // bytes
#define MAX_STATS_SIZE 100000
#define MAX_USERS 5000
#define GET_TIMELINE_RATE 80
#define MAX_PARTS 5

struct client_value
{
  unsigned       unique_client_id; // client unique identifier
  struct timeval t;                // client timestamp
  short          part_nr;          // number of partitions required to run
  size_t         size;             // size of the value in bytes
  short command;  // 1: GET_TIMELINE; 2: POST   ; 4: FOLLOW ; 8: MIGRATE_USER
  int   user_id;  // 1: USER_ID     ; 2: USER_ID; 4: USER_ID; 8: PARTITION "TO"
  char  value[0]; // 1: -------     ; 2: MSG    ; 4: -------; 8: USER_ID LIST
};

struct stats
{
  long delivered;
  long latency;
  long min_latency;
  long max_latency;
  long avg_latency;
};

struct client
{
  int                  client_id;
  unsigned             unique_client_id;
  int                  stats_idx[MAX_PARTS];
  char                 my_post[MAX_MSG_SIZE];
  struct client_value* v;
  struct ssn_msg_t     posts[MAX_POSTS];
  struct stats         stats[MAX_PARTS][MAX_STATS_SIZE];
  struct event_base*   base;
  struct bufferevent*  bev;
  struct event*        stats_ev;
  struct timeval       stats_interval;
  struct timeval       first_connect_ts;
  int                  stop;
};

/**
 * client configuration parameters - default values
 */
static int         group_id = 0;
static int         value_size = 16;
static int         outstanding = 1;
static int         verbose = 0;
static int         freq_stats = 100;
static int         THREADS = 1;
static int         conf_rate = 0;
static const char* stats_file = "stats-client";
static const char* ip = "127.0.0.1";
static const char* bkp_ip = "";
static int         port = 9002;
static int         lu[MAX_USERS] = { 0 }; // array of local users' id
static int         lu_count = 0;
static int         flag_bkp_ip = 0;

#define LATENCY_MODE 1

/**
 * clients threads
 */
static struct client** clients;

static void
save_stats(char* file_name, struct stats* st, int size)
{
  int   i;
  FILE* f = fopen(file_name, "w");
  if (f != NULL) {
    if (LATENCY_MODE)
      fprintf(f, "#D_TIME\tVALUES\tLATENCY\tMIN_LAT\tABS_TIME\tUSER_ID\n");
    else
      fprintf(f, "#D_TIME\tVALUES\tLATENCY\tMIN_LAT\tMAX_LAT\tAVG_LAT\n");

    for (i = 0; i < size; i++)
      fprintf(f, "%d\t%ld\t%ld\t%ld\t%ld\t%ld\n", freq_stats, st[i].delivered,
              st[i].latency, st[i].min_latency, st[i].max_latency,
              st[i].avg_latency);
    fclose(f);
  } else {
    printf("Could not save local stats info!\n");
  }
}

static void
finish_client(struct client* c)
{
  int                i;
  struct event_base* base = c->base;
  char               fname[100];
  c->stop = 1;
  for (i = 0; i < MAX_PARTS; i++) {
    sprintf(fname, "%s-%dpartitions-g%d-t%d.txt", stats_file, (i + 1), group_id,
            c->client_id);
    printf("Saving stats to file '%s'\n", fname);
    save_stats(fname, c->stats[i], c->stats_idx[i]);
  }

  event_base_loopexit(base, NULL);
}

static void
sigint_handler(int sig)
{
  int i;
  printf("Caught signal %d\n", sig);
  for (i = 0; i < THREADS; i++)
    finish_client(clients[i]);
  sleep(1);
}

static void
random_string(char* s, const int len)
{
  int               i;
  static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  for (i = 0; i < len - 1; ++i)
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  s[len - 1] = 0;
}

// Returns t2 - t1 in microseconds.
static long
timeval_diff(struct timeval* t1, struct timeval* t2)
{
  long us;

  us = (t2->tv_sec - t1->tv_sec) * 1e6;
  if (us < 0)
    return 0;
  us += (t2->tv_usec - t1->tv_usec);
  return us;
}

static void
update_stats(struct stats* stats, struct client_value* delivered)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  long lat = timeval_diff(&delivered->t, &tv);

  stats->delivered++;
  stats->latency = lat;
  stats->avg_latency =
    stats->avg_latency + ((lat - stats->avg_latency) / stats->delivered);
  if (stats->min_latency == 0 || lat < stats->min_latency)
    stats->min_latency = lat;
  if (lat > stats->max_latency)
    stats->max_latency = lat;
}

static void
on_stats(evutil_socket_t fd, short event, void* arg)
{
  struct client* c = arg;
  int            i = 0;

  for (; i < MAX_PARTS; i++)
    if (c->stats_idx[i] < MAX_STATS_SIZE - 1)
      c->stats_idx[i]++;

  event_add(c->stats_ev, &c->stats_interval);
}

static void
client_submit_value(struct client* c)
{
  m_gid_t dst[1] = { group_id };
  int     dst_len = 1;

  if (rand() % 100 < conf_rate)
    c->v->user_id = lu[0];
  else
    c->v->user_id = lu[rand() % lu_count];

  gettimeofday(&c->v->t, NULL);
  c->v->command = (rand() % 100) < GET_TIMELINE_RATE ? 1 : 2;
  switch (c->v->command) {
    case 1: // get timeline
      c->v->size = 0;
      break;
    case 2: // post
      c->v->size = value_size;
      memcpy(c->v->value, c->my_post, value_size);
      break;
    default:
      printf("Invalid command. Not sending.\n");
      return;
  }
  size_t size = sizeof(struct client_value) + c->v->size;
  mcast_submit(c->bev, (char*)c->v, size, dst, dst_len);

  if (verbose)
    printf("Client %d / %u from group %d: command '%d', user %d, size %zu.\n",
           c->client_id, c->unique_client_id, group_id, c->v->command,
           c->v->user_id, size);
}

static struct bufferevent* connect_to_node(struct client* c,
                                           const char*    str_addr);

static void
on_connect(struct bufferevent* bev, short events, void* arg)
{
  struct client* c = arg;
  int            i = 0;

  if (events & BEV_EVENT_CONNECTED) {
    printf("Client %d: connected to node...\n", c->client_id);
    assert(outstanding > 0);
    for (; i < outstanding; i++) {
      client_submit_value(c);
    }
  } else {
    printf("Socket ERROR: %s.... Trying to reconnect\n",
           evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
    bufferevent_free(c->bev);
    if (strcmp(bkp_ip, "")) {
      c->bev = connect_to_node(c, bkp_ip);
      if (flag_bkp_ip)
        sleep(2);
      else
        flag_bkp_ip = 1;

    } else {
      sleep(2);
      c->bev = connect_to_node(c, ip);
    }
  }
}

static void
on_read(struct bufferevent* bev, void* ctx)
{

  struct client*      c = ctx;
  int                 idx, qty = 0;
  struct client_value v;
  size_t size = bufferevent_read(bev, &v, sizeof(struct client_value)),
         offset = 0;

  if (v.size) {
    offset = bufferevent_read(bev, c->posts, v.size);
  }

  assert(offset == v.size);
  idx = v.part_nr - 1;

  if (LATENCY_MODE) {
    struct stats*  stats = &(c->stats[idx][c->stats_idx[idx]]);
    struct timeval tv;
    gettimeofday(&tv, NULL);
    long lat = timeval_diff(&v.t, &tv);
    stats->delivered = 1;
    stats->latency = stats->min_latency = lat;
    stats->max_latency = timeval_diff(&c->first_connect_ts, &tv);
    stats->avg_latency = v.user_id;
    if (c->stats_idx[idx] < MAX_STATS_SIZE - 1)
      c->stats_idx[idx]++;
  } else {
    update_stats(&(c->stats[idx][c->stats_idx[idx]]), &v);
  }

  if (!c->stop)
    client_submit_value(c);

  if (verbose) {
    printf("Client %d: received %zu bytes - command %d on user %d using %d "
           "partition(s)\n",
           c->client_id, size, v.command, v.user_id, v.part_nr);

    if (v.command == 1) { // GET_TIMELINE
      qty = v.size / sizeof(struct ssn_msg_t);
      printf("%d message(s):\n", qty);
      for (idx = 0; idx < qty; idx++) {
        printf("user '%d': '%s'\n", c->posts[idx].user_id, c->posts[idx].msg);
      }
    }
  }
}

static struct bufferevent*
connect_to_node(struct client* c, const char* str_addr)
{
  struct bufferevent* bev;
  struct sockaddr_in  addr;
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = inet_addr(str_addr);
  bev = bufferevent_socket_new(c->base, -1,
                               BEV_OPT_CLOSE_ON_FREE); // | BEV_OPT_THREADSAFE);
  bufferevent_setcb(bev, on_read, NULL, on_connect, c);
  bufferevent_enable(bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(bev, (struct sockaddr*)&addr, sizeof(addr));
  int flag = 1;
  setsockopt(bufferevent_getfd(bev), IPPROTO_TCP, TCP_NODELAY, &flag,
             sizeof(int));
  return bev;
}

static struct client*
make_client(int client_id)
{
  struct client* c;
  c = malloc(sizeof(struct client));
  c->base = event_base_new();
  c->client_id = client_id;
  c->unique_client_id =
    (client_id << (rand() % 10)) + (rand() % 1000003) + (rand() % 1000001);
  c->stop = 0;
  random_string(c->my_post, value_size);
  memset(c->stats_idx, 0, MAX_PARTS * sizeof(int));
  c->v = malloc(sizeof(struct client_value) + MAX_VALUE_SIZE);
  c->v->unique_client_id = c->unique_client_id;
  c->bev = connect_to_node(c, ip);
  if (c->bev == NULL)
    exit(1);

  memset(&(c->stats), 0, MAX_PARTS * MAX_STATS_SIZE * sizeof(struct stats));
  if (!LATENCY_MODE) {
    c->stats_interval =
      (struct timeval){ freq_stats / 1000, (freq_stats * 1000) % 1000000 };
    c->stats_ev = evtimer_new(c->base, on_stats, c);
    event_add(c->stats_ev, &c->stats_interval);
  }
  return c;
}

static void
client_free(struct client* c)
{
  bufferevent_free(c->bev);
  if (!LATENCY_MODE) {
    event_free(c->stats_ev);
  }
  event_base_free(c->base);
  free(c->v);
  free(c);
}

static void*
start_thread(void* v)
{
  int client_id = *((int*)v);
  printf("Client %d: starting...\n", client_id);
  struct client* client;
  client = make_client(client_id);
  clients[client_id] = client;
  gettimeofday(&client->first_connect_ts, NULL);
  event_base_dispatch(client->base);
  client_free(client);
  pthread_exit(NULL);
}

static void
start_client(int* client_id, pthread_t* t)
{
  int rc = 0;
  if ((rc = pthread_create(t, NULL, start_thread, client_id))) {
    fprintf(stderr, "error: pthread_create, rc: %d\n", rc);
  }
}

void
load_users(const char* file)
{
  FILE* f = fopen(file, "r");
  if (f == NULL) {
    printf("Error loading user ids from file '%s'.\n", file);
    exit(2);
  }
  while (!feof(f) && lu_count < MAX_USERS) {
    fscanf(f, "%d\n", &lu[lu_count++]);
  }
  fclose(f);
  printf("Loaded %d user ids.\n", lu_count);
}

static void
usage(const char* name)
{
  printf("Usage: %s </path/to/my-group-user-list.txt> -g <group> -r "
         "<replica-ip> -p <replica-port>  -s <post-size> -f <stats-per-second> "
         "-t <threads> -o <outstanding-messages> -c <conflict-rate> -b "
         "<secondary-replica-ip> -v\n",
         name);
  printf("  %-30s%s\n", "-h, --help", "Output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose", "increase verbosity");
  printf("  %-30s%s\n", "-g, --group #", "id of client's local partition'");
  printf("  %-30s%s\n", "-r, --replica-ip #", "address of the replica");
  printf("  %-30s%s\n", "-p, --replica-port #", "port of the replica");
  printf("  %-30s%s\n", "-s, --value-size #",
         "Size of client post (in bytes - max 64B)");
  printf("  %-30s%s\n", "-f, --freq-stats #", "number of stats per second");
  printf("  %-30s%s\n", "-t, --threads #",
         "number of client threads submitting concurrently (default = 1)");
  printf("  %-30s%s\n", "-o, --outstanding #",
         "number of outstanding messages per thread (default = 1)");
  printf("  %-30s%s\n", "-c, --conflict-rate #",
         "number of messages with the same 'key' in order to impose a conflict "
         "(default = 0)");
  printf("  %-30s%s\n", "-b, --backup-replica-ip #",
         "IP address of a second replica to connect to in case of failure "
         "(default = NONE)");
  exit(1);
}

int
main(int argc, char const* argv[])
{
  int        i = 1;
  int*       ids;
  pthread_t* t;

  if (argc < 2)
    usage(argv[0]);

  load_users(argv[i++]);

  while (i != argc) {
    if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
      usage(argv[0]);
    else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0)
      verbose = 1;
    else if (strcmp(argv[i], "-g") == 0 || strcmp(argv[i], "--group") == 0)
      group_id = atoi(argv[++i]);
    else if (strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--replica-ip") == 0)
      ip = argv[++i];
    else if (strcmp(argv[i], "-b") == 0 ||
             strcmp(argv[i], "--backup-replica-ip") == 0)
      bkp_ip = argv[++i];
    else if (strcmp(argv[i], "-p") == 0 ||
             strcmp(argv[i], "--replica-port") == 0)
      port = atoi(argv[++i]);
    else if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "--post-size") == 0)
      value_size = atoi(argv[++i]);
    else if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--freq-stats") == 0)
      freq_stats = 1000 / atoi(argv[++i]);
    else if (strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--threads") == 0)
      THREADS = atoi(argv[++i]);
    else if (strcmp(argv[i], "-o") == 0 ||
             strcmp(argv[i], "--outstanding") == 0)
      outstanding = atoi(argv[++i]);
    else if (strcmp(argv[i], "-c") == 0 ||
             strcmp(argv[i], "--conflict-rate") == 0)
      conf_rate = atoi(argv[++i]);
    else
      usage(argv[0]);
    i++;
  }

  srand(time(NULL));
  signal(SIGINT, sigint_handler);
  t = malloc(THREADS * sizeof(pthread_t));
  ids = malloc(THREADS * sizeof(int));
  clients = malloc(THREADS * sizeof(struct client*));
  i = evthread_use_pthreads();
  assert(i == 0);
  for (i = 0; i < THREADS; i++) {
    ids[i] = i;
    start_client(&ids[i], &t[i]);
  }

  for (i = 0; i < THREADS; ++i) {
    pthread_join(t[i], NULL);
    printf("Client %d finished!\n", ids[i]);
  }
  free(t);
  free(ids);
  free(clients);
  return 0;
}
